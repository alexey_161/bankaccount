//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WpfAppBankAccount
{
    using System;
    using System.Collections.Generic;
    
    public partial class Account
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int BankId { get; set; }
        public int AggrementId { get; set; }
        public string Account1 { get; set; }
    
        public virtual Aggrement Aggrement { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual TypeAccount TypeAccount { get; set; }
    }
}
